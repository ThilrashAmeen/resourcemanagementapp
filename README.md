# ResourceManagementApp

Java Project for the ITPM Module. Technology used JavaFX, MySql.  

ABC is a leading non-state degree awarding institute. Assume your group is working in the IT division of the ABC institute. You 
have been asked to develop a Resource Management application for managing the resources of the ABC institute. The main 
functions and features of the system are as follows:
Section 1
This section includes details related to the working days and hours, lecturers, subjects, students, tags, and locations.
• The developed system should include an interface which facilitates the following entries related to the working days and hours:
▪ Adding, editing, and removing the number of working days per week (Eg: 3) 
▪ Adding, editing, and removing the working days (Eg: Monday, Tuesday, and Wednesday)
▪ Adding the time slots of the timetable. Should facilities the addition of one of the following time slots:
o One hour time slots (Eg: 13.00 -14.00)
o Thirty minutes time slots (Eg: 13.30 -14.00)
Fig. 1: Sample interface to display the working days and hours
• The developed system should include an interface which facilitates the following entries related to the lecturers:
▪ Adding the following lecturer details:
o Name
o Employee ID. This should be 6 digit number (Eg: 000150).
o Faculty (Eg: Computing, Engineering, Business, Humanities & Sciences, etc.)
o Department
o Campus/Center (Eg: Malabe, Metro, Matara, Kandy, Kurunagala, and Jaffna)
o Building (Eg: New building, D-block etc.)
o Level. The level should be assigned as follows: Category Level
Professor 1
Assistant Professor 2
Senior Lecturer(HG) 3
Senior Lecturer 4
Lecturer 5
Assistant Lecturer 6
o Rank. The rank is a combination of the level and employee ID. It is defined as follows: level.employee ID 
(Eg: 2.000150). Accordingly, when deciding on a time slot, from the staff members who have requested for that 
slot, the staff member with the lowest rank should be allocated that slot.
▪ Editing lecturer details
▪ Removing lecturers
▪ Assigning active hours of lecturers(Some lecturers would not be available in particular days and hours)
▪ Viewing added details of lecturers
Fig. 2: Sample interface to display adding lectures
Fig. 3: Sample interface to managing lectures▪ The developed system should include an interface which facilitates the following entries related to the subjects:
▪ Adding the following details related to the subjects:
o Offered year
o Offered semester
o Subject name
o Subject code
o Number of lecture hours (Eg: 02)
o Number of tutorial hours (Eg: 01)
o Number of lab hours (Eg: 00)
o Number of evaluation hours (Eg: 02)
▪ Editing subject details
▪ Removing subjects
▪ Viewing added details of subjects
Fig. 4: Sample interface for adding subjects
Fig. 5: Sample interface for managing subjects▪ The developed system should include an interface which facilitates the following entries related to the students:
▪ Adding the academic year and semester (Eg: Y1.S1, Y1.S2, Y2.S1, Y2.S2, Y3.S1, Y3.S2, Y4.S1, and Y4.S2)
▪ Editing the academic year and semester 
▪ Removing the academic year and semester
▪ Adding the programme (Eg :IT/CSSE/CSE/IM) 
▪ Editing the programme (Eg :IT/CSSE/CSE/IM) 
▪ Removing the programme (Eg :IT/CSSE/CSE/IM) 
▪ Adding group numbers (Eg: 01, 02, 03 etc.)
▪ Editing group numbers
▪ Removing group numbers
▪ Generating group IDs. Group ID is defined as follows: 
o Year.semester.programme.group number (Eg: Y1.S1.IT.01)
▪ Removing generated group IDs
▪ Adding sub-group numbers (Eg: 1, 2, 3 etc.)
▪ Editing sub-group numbers
▪ Removing sub-group numbers
▪ Generating sub-group IDs. Sub-group ID is defined as follows: 
o Year.semester.programme.group number.sub-group number (Eg: Y1.S1.IT.01.1)
▪ Removing generated sub-group IDs
▪ Viewing added details of students
Fig. 5: Sample interface for adding student groupsFig. 6: Sample interface for managing student groups
▪ The developed system should include an interface which facilitates the following entries related to the tags:
▪ Adding tags (Eg: Lecture, tutorial, and practical)
▪ Editing tags
▪ Removing tags
▪ Viewing added details of tags
Fig. 7: Sample interface for adding tags
Fig. 8: Sample interface for managing tags▪ The developed system should include an interface which facilitates the following entries related to the locations:
▪ Adding buildings (Eg: New building, D-block etc.)
▪ Adding rooms (Eg: A501, B502, N3B-PcLab) and their capacities building-wise. A room can be a lecture hall or a 
laboratory.
▪ Editing buildings
▪ Editing rooms
▪ Removing buildings
▪ Removing rooms
▪ Viewing added details of locations
Fig. 9: Sample interface for adding locations
Fig. 10: Sample interface for managing locationsSection 2
▪ The developed system should include interfaces to visualize the following statistics
▪ Statistics related to lecturers
▪ Statistics related to students
▪ Statistics related to subjects
Fig. 11: Sample interface for visualizing statistic
• Add sessions. Steps related to adding of sessions are as follows:
▪ Load lecturers and select the relevant lecturer for the session
▪ Load tags and select the relevant tags for the session
▪ Load students and select the relevant group or sub-group for the session
▪ Load subjects and select the relevant subject for the session
▪ Add the number of students for the session
▪ Add the duration for the session
▪ Finally add the session with the loaded specifications above
• List or visualize the sessions in detail.
• Add filters to search the sessions based on a particular lecturer, year, etc.
Note: A session should include the following:
▪ Lecturer
▪ Subject code
▪ Subject 
▪ Tag (Eg: Lecture, Tutorial, Practical)
▪ Group ID (if the tag is a lecture or tutorial) or sub-group ID (if the tag is a practical)
▪ Student count
▪ Duration (Number of hours for the session)
• Accordingly, the format of a generated session should be as follows:
▪ Dr. Nuwan Kodagoda – IT2030 – OOC – Lecture – Y1.S1.IT.01 – 120 - 2Fig. 12: Sample interface for adding sessions
Fig. 13: Sample interface for adding sessions
Fig. 14: Sample interface for managing session
